package com.akyumen.actor.example.tree.Message;

import java.util.LinkedList;
import java.util.UUID;

import com.akyumen.actor.impl.messages.Message;

public class EstimateResponseMsg extends Message {
	protected LinkedList<Integer> estimate;

	public LinkedList<Integer> getEstimate() {
		return estimate;
	}

	public EstimateResponseMsg(UUID source, UUID destination) {
		super(source, destination, MessageType.Response);
	}

	@SuppressWarnings("unchecked")
	public void updateGuess(LinkedList<Integer> childEstimate, Integer guess) {
		estimate = (LinkedList<Integer>) childEstimate.clone();
		if (guess != null) {
			estimate.addLast(guess);
		}
	}

	@Override
	public String toString() {

		return "EstimateResponseMsg -" + super.toString() + " estimate:" + getEstimateString();

	}

	public String getEstimateString() {
		StringBuilder sb = new StringBuilder();
		if (estimate == null) {
			sb.append("null");
		} else {
			for (Integer est : estimate) {
				sb.append("[" + est + "]");
			}
		}
		return sb.toString();
	}
}
