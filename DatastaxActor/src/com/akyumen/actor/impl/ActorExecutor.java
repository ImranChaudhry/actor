package com.akyumen.actor.impl;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.akyumen.actor.impl.definition.iActorExecutable;
/**
 * Class executes actors in its threadpool - each actor can run through all of its messages or it
 * can be told the number of maximum messages it can process per iteration.
 * After the end of the iteration it returns true to be re-scheduled and false if it has terminated
 * It gets then added (if rescheduled) to the bottom of the actor queue and gets re-run.
 * This is to prevent thread lock where lock running actors starve the other actors and so nothing
 * can finish
 * @author imran
 *
 */

public class ActorExecutor implements Runnable {
	private static final Logger logger = Logger.getLogger("com.akyumen.actor.impl");
	protected int maxNumberThreads;
	protected AtomicInteger numberThreads = new AtomicInteger(0);
	//FIFO queue
	ConcurrentLinkedQueue<iActorExecutable> queuedActors;
	ExecutorService executor;

	/**
	 * Specifies the maximum number of threads the ActorExecutor can use
	 * @param maxNumberThreads
	 */
	public ActorExecutor(int maxNumberThreads) {
		super();
		this.maxNumberThreads = maxNumberThreads;
		//simple FIFO queue
		queuedActors = new ConcurrentLinkedQueue<iActorExecutable>();
		executor = Executors.newFixedThreadPool(maxNumberThreads);
	}
	
	/**
	 * Takes an actor and schedules it for execution
	 * @param actor
	 */

	public void schedule(iActorExecutable actor) {

		logger.log(Level.FINEST, "Scheduling actor" + actor.toString());
		queuedActors.add(actor);
		balanceExecutors();
	}

	/*
	 * Increases the number of threads if we have more actors than 3x the number of threads running
	 * Capped at the maximum number of threads
	 */
	protected void balanceExecutors() {
		

		if (numberThreads.get() < Math.min(maxNumberThreads, (queuedActors.size()/3.0))) {
			//start a new thread if we have more than 3 * actors than threads (heuristic for load balancing)
			
			executor.submit(this);
		}
	}

	
	@Override
	/*
	 * Method which runs the actor consumer thread - gets the next actor runs it and if it wants to
	 * be rescheduled - reschedules it for later. 
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		int numThreads = numberThreads.incrementAndGet();
		Thread.currentThread().setName("Actor Executor " + numThreads);
		logger.log(Level.FINEST, "Starting new thread : Actor Executor " + numThreads);
		while (true) {
			// get next waiting actor
			iActorExecutable actor = queuedActors.poll();
			if (actor == null) {// time to exit as there are no waiting actors
				logger.log(Level.FINEST, "Exiting Thread (no waiting actors): Actor Executor " + numThreads);
				numberThreads.decrementAndGet();
				return;
			} else {
				if (actor.executeMessages()) {
					// if true - it means reschedule to be processed again
					// if false - dispose of the actor
					schedule(actor);
				}
			}
		}
	}

	public void shutdown() {
		logger.log(Level.INFO, "Shutdown called");
		executor.shutdown();
		
	}

}
