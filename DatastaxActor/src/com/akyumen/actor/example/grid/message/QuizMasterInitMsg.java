package com.akyumen.actor.example.grid.message;

import java.util.UUID;

import com.akyumen.actor.impl.messages.Message;

public class QuizMasterInitMsg extends Message {
	public int getCodeLength() {
		return codeLength;
	}

	public int getCodeMin() {
		return codeMin;
	}

	public int getCodeMax() {
		return codeMax;
	}

	protected int codeLength;
	protected int codeMin;
	protected int codeMax;

	public QuizMasterInitMsg(UUID source, UUID destination, int codeLength, int codeMin, int codeMax) {
		super(source, destination, Message.MessageType.Initialise);
		this.codeLength = codeLength;
		this.codeMax = codeMax;
		this.codeMin = codeMin;
	}

	@Override
	public String toString() {

		return "QuizMasterInitMsg -"+super.toString()+" codeLength:" + codeLength + " codeMax: " + codeMax + " codeMin: " + codeMin;
	}

}
