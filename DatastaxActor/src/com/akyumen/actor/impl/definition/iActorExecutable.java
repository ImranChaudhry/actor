package com.akyumen.actor.impl.definition;

public interface iActorExecutable {
	
	public boolean executeMessages();

}
