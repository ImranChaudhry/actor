package com.akyumen.actor.impl.messages;

import java.util.UUID;

public class UnknownRequestMsg extends Message {

	String reason;

	public UnknownRequestMsg(UUID source, UUID destination, String reason) {
		super(source, destination, Message.MessageType.Response);
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "UnknownRequestMsg for "+super.toString()+" Reason : " + reason;
	}
}
