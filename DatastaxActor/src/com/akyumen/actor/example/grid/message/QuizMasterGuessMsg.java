package com.akyumen.actor.example.grid.message;

import java.util.UUID;

import com.akyumen.actor.impl.messages.Message;

public class QuizMasterGuessMsg extends Message {

	public QuizMasterGuessMsg(UUID source, UUID destination) {
		super(source, destination, MessageType.Request);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "QuizMasterGuessMsg "+super.toString();
	}

}
