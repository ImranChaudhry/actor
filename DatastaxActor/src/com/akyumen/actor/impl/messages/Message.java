package com.akyumen.actor.impl.messages;

import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * All Messages must extend this class and enumerate their MessageType
 * @author imran
 *
 */
public abstract class Message implements Comparable<Message> {
	public interface AdvisoryNotification {
		// Tags message that if it can't be delivered drop it and don't tell
		// sender
	}
	public enum MessageType {
		Infrastructure, Initialise, Response, Request
		// Infrastructure has priority
		// The initialisation of actors
		// Deal with responses to Request prior to Requests for more work
	};

	private static final Logger logger = Logger.getLogger("com.akyumen.actor.impl");

	protected MessageType msgType;
	protected UUID source;
	protected UUID destination;
	protected boolean initialised = false;

	public Message(UUID source, UUID destination, Message.MessageType msgType) {

		this.source = source;
		this.destination = destination;
		this.msgType = msgType;
		logger.log(Level.FINEST, "Constructor called for " + toString());
	}

	public MessageType getMsgType() {
		return msgType;
	}

	public UUID getSource() {
		return source;
	}

	public UUID getDestination() {
		return destination;
	}

	public String toString() {
		return String.format("Source: %s Destination: %s MsgType: %s", source, destination, msgType);
	}

	@Override
	public int compareTo(Message obj) {
		if (obj!=null){
		return this.msgType.ordinal() - obj.msgType.ordinal();
		}
		return 1; //I am greater than null
	}
}
