package com.akyumen.actor.example.tree.Message;

import java.util.LinkedList;
import java.util.UUID;

import com.akyumen.actor.impl.messages.Message;

public class EstimateRequestMsg extends Message {
	protected int depth;
	protected LinkedList<Integer> estimate;

	public LinkedList<Integer> getEstimate() {
		return estimate;
	}

	public int getDepth() {
		return depth;
	}

	public EstimateRequestMsg(UUID source, UUID destination, int depth) {
		super(source, destination, MessageType.Request);
		this.depth = depth;
	}

	@SuppressWarnings("unchecked")
	public void updateGuess(LinkedList<Integer> childEstimate, Integer guess) {
		if (childEstimate == null)
		{
			estimate = new LinkedList<Integer>(); 
		}
		else{
		estimate = (LinkedList<Integer>) childEstimate.clone();
		}
		if (guess != null) {
			estimate.addLast(guess);
		}
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "EstimateRequestMsg - " + super.toString() + "  Depth:" + depth;
	}
}
