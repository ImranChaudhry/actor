package com.akyumen.actor.example.tree;

import java.util.LinkedList;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.akyumen.actor.example.grid.message.EstimatorInitMsg;
import com.akyumen.actor.example.grid.message.QuizMasterGuessMsg;
import com.akyumen.actor.example.tree.Message.EstimateRequestMsg;
import com.akyumen.actor.example.tree.Message.EstimateResponseMsg;
import com.akyumen.actor.impl.definition.iActorPool;
import com.akyumen.actor.impl.messages.Message;
import com.akyumen.actor.impl.messages.SelfDestructMsg;

/**
 * Simple example - makes a tree of guessers which guess the code so a 4 digit
 * code from 3-9 would be codeLength=4, codeMin=3 codeMax=9 and result in a 5
 * hierarchy tree of Estimators (1 to coordinate - 4 to guess each digit) Guess
 * Requests propogate down the tree, each time appending the guess to
 * guessRequest and at the leaf the guess is returned directly to the
 * QuizMaster. After each guess - the quizmaster terminates that Estimator. Once
 * an estimator has no children left it terminates itself
 */

public class QuizMaster extends com.akyumen.actor.example.grid.QuizMaster {
	static {
		logger = Logger.getLogger("com.akyumen.actor.example2");
	}

	public QuizMaster(UUID uuid, UUID parent, iActorPool pool) {
		super(uuid, parent, pool);
	}

	@Override
	public boolean processRequest(Message msg) throws Throwable {
		if (msg instanceof QuizMasterGuessMsg) {
			logger.log(Level.FINER, "QuizMasterGuessMsg received" + msg);
			if (child == null) { // initialise on first guess only
				child = spawnActor(Estimator.class);
				sendMessage(new EstimatorInitMsg(getUuid(), child, codeMin, codeMax, null));
			}
			sendMessage(new EstimateRequestMsg(getUuid(), child, codeLength));
			return true;
		}
		return false;
	}

	@Override
	public boolean processResponse(Message msg) throws Throwable {
		if (msg instanceof EstimateResponseMsg) {
			logger.log(Level.FINER, "EstimateResponseMsg received " + msg);
			EstimateResponseMsg erMsg = (EstimateResponseMsg) msg;
			boolean found = true;
			LinkedList<Integer> guess = erMsg.getEstimate();
			for (int i = 0; i < codeLength; i++) {
				if (guess.get(i) != code[i]) {
					found = false;
					break;
				}
			}
			if (found) {
				StringBuilder sb = new StringBuilder();
				sb.append("Code Match : ");
				for (int num : code) {
					sb.append("[" + num + "]");
				}

				logger.log(Level.INFO, sb.toString());
				// kill myself
				sendMessage(new SelfDestructMsg(getUuid(), getUuid()));
			} else {
				StringBuilder sb = new StringBuilder();
				sb.append("Code Mismatch : Actual ");
				for (int num : code) {
					sb.append("[" + num + "]");
				}
				sb.append(" Guess: ");
				for (int num : guess) {
					sb.append("[" + num + "]");
				}

				logger.log(Level.INFO, sb.toString());
				// kill the guesser
				sendMessage(new SelfDestructMsg(getUuid(), msg.getSource()));

			}
			return true;
		}
		// terminate myself
		return false;

	}

	@Override
	public void notifyChildrenModification(UUID deadChild) {
		if (children.size() == 0) { // if no children - kill myself
			sendMessage(new SelfDestructMsg(getUuid(), getUuid()));
		}
	}

}
