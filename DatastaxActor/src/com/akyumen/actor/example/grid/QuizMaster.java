package com.akyumen.actor.example.grid;

import java.util.LinkedList;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.akyumen.actor.example.grid.message.EstimateRequestMsg;
import com.akyumen.actor.example.grid.message.EstimateResponseMsg;
import com.akyumen.actor.example.grid.message.EstimatorInitMsg;
import com.akyumen.actor.example.grid.message.QuizMasterGuessMsg;
import com.akyumen.actor.example.grid.message.QuizMasterInitMsg;
import com.akyumen.actor.impl.Actor;
import com.akyumen.actor.impl.definition.iActorPool;
import com.akyumen.actor.impl.messages.Message;
import com.akyumen.actor.impl.messages.SelfDestructMsg;

/**
 * Simple example - makes a tree of guessers which guess the code so a 4 digit
 * code from 3-9 would be codeLength=4, codeMin=3 codeMax=9 and result in a 5
 * hierarchy tree of Estimators (1 to coordinate - 4 to guess each digit) Guess
 * Requests propogate down the tree and at the leaf the guess returns to its
 * parents - each time appending the parents guess and eventually back to the
 * quizmaster Once the correct code is guessed - the head Estimator is killed,
 * which initiated killing of its children
 */

public class QuizMaster extends Actor {
	protected static Logger logger = Logger.getLogger("com.akyumen.actor.example1");
	protected int codeLength;
	protected int codeMin;
	protected int codeMax;
	protected int[] code;
	protected UUID child;

	public QuizMaster(UUID uuid, UUID parent, iActorPool pool) {
		super(uuid, parent, pool);
	}

	@Override
	public boolean processRequest(Message msg) throws Throwable {
		if (msg instanceof QuizMasterGuessMsg) {
			logger.log(Level.FINER, "QuizMasterGuessMsg received" + msg);
			if (child == null) { // initialise on first guess only
				child = spawnActor(Estimator.class);
				sendMessage(new EstimatorInitMsg(getUuid(), child, codeMin, codeMax, null));
			}
			sendMessage(new EstimateRequestMsg(getUuid(), child, codeLength));
			return true;
		}
		return false;
	}

	@Override
	public boolean processResponse(Message msg) throws Throwable {
		if (msg instanceof EstimateResponseMsg) {
			logger.log(Level.FINER, "EstimateResponseMsg received " + msg);
			EstimateResponseMsg erMsg = (EstimateResponseMsg) msg;
			boolean found = true;
			LinkedList<Integer> guess = erMsg.getEstimate();
			for (int i = 0; i < codeLength; i++) {
				if (guess.get(i) != code[i]) {
					found = false;
					break;
				}
			}
			if (found) {
				StringBuilder sb = new StringBuilder();
				sb.append("Code Match : ");
				for (int num : code) {
					sb.append("[" + num + "]");
				}

				logger.log(Level.INFO, sb.toString());
				sendMessage(new SelfDestructMsg(getUuid(), child));
				// terminate myself
				sendMessage(new SelfDestructMsg(getUuid(), getUuid()));
			}

			return true;
		}
		return false;

	}

	@Override
	public boolean initialise(Message msg) throws Throwable {
		if (msg instanceof QuizMasterInitMsg) {
			logger.log(Level.FINER, "QuizMasterInitMsg received " + msg);
			QuizMasterInitMsg qm = (QuizMasterInitMsg) msg;
			codeLength = qm.getCodeLength();
			codeMax = qm.getCodeMax();
			codeMin = qm.getCodeMin();
			int range = codeMax - codeMin + 1;
			code = new int[codeLength];
			StringBuffer sb= new StringBuffer();
			for (int i = 0; i < codeLength; i++) {// generate secret code for
													// guessing
				code[i] = (int) (codeMin + range * Math.random());
				sb.append("["+code[i]+"]");
			}
			logger.log(Level.FINER, "Code Generated " + sb.toString());

			return true;
		} else {
			return false;
		}
	}

	@Override
	public void notifyChildrenModification(UUID deadChild) {
		// Do Nothing I don't care if children die
	}

}
