package com.akyumen.actor.impl.messages;

import java.util.UUID;

public class ActorFailureMsg extends Message implements Message.AdvisoryNotification{

	String reason;
	public ActorFailureMsg (UUID source, UUID destination, String reason){
		super(source, destination, Message.MessageType.Infrastructure);
		this.reason= reason;
	}

	@Override
	public String toString() {
		return "ActorFailureMsg "+super.toString()+ " Reason : " + reason;
	}

}
