package com.akyumen.actor.example.grid.message;

import java.util.UUID;

import com.akyumen.actor.impl.messages.Message;

public class EstimatorInitMsg extends Message {

	public Integer getCodeGuess() {
		return codeGuess;
	}

	public int getCodeMin() {
		return codeMin;
	}

	public int getCodeMax() {
		return codeMax;
	}

	protected Integer codeGuess;
	protected int codeMin;
	protected int codeMax;

	public EstimatorInitMsg(UUID source, UUID destination, int codeMin, int codeMax, Integer codeGuess) {
		super(source, destination, MessageType.Initialise);
		this.codeMax = codeMax;
		this.codeMin = codeMin;
		this.codeGuess = codeGuess;
	}

	@Override
	public String toString() {
		return "EstimatorInitMsg "+super.toString()+"  codeGuess:" + codeGuess + " - codeMax:" + codeMax + " - codeMin:" + codeMin;
	}

}
