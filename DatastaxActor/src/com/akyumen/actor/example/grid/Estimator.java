package com.akyumen.actor.example.grid;

import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.akyumen.actor.example.grid.message.EstimateRequestMsg;
import com.akyumen.actor.example.grid.message.EstimateResponseMsg;
import com.akyumen.actor.example.grid.message.EstimatorInitMsg;
import com.akyumen.actor.impl.Actor;
import com.akyumen.actor.impl.definition.iActorPool;
import com.akyumen.actor.impl.messages.Message;

public class Estimator extends Actor {
	protected static Logger logger = Logger.getLogger("com.akyumen.actor.example1");

	protected int codeMin;
	protected int codeMax;
	protected Integer guess;
	protected volatile UUID[] childEstimators;

	public Estimator(UUID uuid, UUID parent, iActorPool pool) {
		super(uuid, parent, pool);
	}

	@Override
	public boolean processRequest(Message msg) throws Throwable {
		if (msg instanceof EstimateRequestMsg) {
			logger.log(Level.FINE, "EstimateRequestMsg Received " + msg);
			int depth = ((EstimateRequestMsg) msg).getDepth();
			if (guess != null) { // initial step - ignore when no guess as I am
									// a consolidator and not an estimator
				depth--;
			}
			if (depth == 0) {
				logger.log(Level.FINER, "Depth is 0 sending EstimateResponseMsg to " + parent + "with guess of "
						+ guess);
				EstimateResponseMsg er = new EstimateResponseMsg(getUuid(), parent);
				er.updateGuess(guess);
				sendMessage(er);

			} else {
				spawnChildren();

				int range = codeMax - codeMin + 1;
				for (int i = 0; i < range; i++) {
					logger.log(Level.FINER, "Depth is " + depth + " sending EstimateRequestMsg to " + childEstimators[i]);
					sendMessage(new EstimateRequestMsg(getUuid(), childEstimators[i], depth));
				}
			}
			return true;
		}
		return false;

	}

	@Override
	public boolean processResponse(Message msg) throws Throwable {
		if (msg instanceof EstimateResponseMsg) {
			logger.log(Level.FINER, "EstimateResponseMsg received - Message: " + msg);
			EstimateResponseMsg childResp = (EstimateResponseMsg) msg;
			EstimateResponseMsg er = new EstimateResponseMsg(getUuid(), parent);
			er.updateGuess(childResp.getEstimate(), guess);
			sendMessage(er);
			logger.log(Level.FINER, "EstimateResponseMsg sent - Message: " + er);
			return true;
		}
		return false;
	}

	protected void spawnChildren() {
		if (childEstimators == null) {
			synchronized (this) {
				if (childEstimators == null) {
					int range = codeMax - codeMin + 1;
					childEstimators = new UUID[range];
					for (int i = 0; i < range; i++) {
						childEstimators[i] = spawnActor(Estimator.class);
						logger.log(Level.FINER, "Spawning Estimator actor as a child : " + childEstimators[i]);
						sendMessage(new EstimatorInitMsg(getUuid(), childEstimators[i], codeMin, codeMax, codeMin + i));
					}
				}
			}
		}
	}

	@Override
	public boolean initialise(Message msg) throws Throwable {
		if (msg instanceof EstimatorInitMsg) {
			EstimatorInitMsg qm = (EstimatorInitMsg) msg;
			codeMax = qm.getCodeMax();
			codeMin = qm.getCodeMin();
			guess = qm.getCodeGuess();
			logger.log(Level.FINER, "Initialising : " + msg);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void notifyChildrenModification(UUID deadChild) {
		// Do Nothing I don't care if children die
	}

}
