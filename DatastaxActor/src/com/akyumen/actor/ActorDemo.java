package com.akyumen.actor;

import java.util.UUID;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.akyumen.actor.example.grid.message.QuizMasterGuessMsg;
import com.akyumen.actor.example.grid.message.QuizMasterInitMsg;
import com.akyumen.actor.impl.ActorPool;

public class ActorDemo {
	
	/** Demonstrates Actor Infrastructure using three demo:
	 * Tree, Grid and Recursive - full details of which are in the respective
	 * Quizmaster classes in their packages.
	 * @param args
	 */

	public static void main(String[] args) {
		Level consoleLevel = Level.FINER; 

		Logger logger = Logger.getLogger("com.akyumen.actor");
		logger.setUseParentHandlers(false);
		logger.setLevel(consoleLevel);
		ConsoleHandler ch = new ConsoleHandler();
		ch.setLevel(consoleLevel);
		logger.addHandler(ch);
		logger.log(Level.INFO, "Starting Demo");

		int codeDepth = 4;
		int minRange = 1;
		int maxRange = 7;
		int numThreads = 16;
		if (args.length == 3 || args.length == 4) {
			codeDepth = Integer.parseInt(args[0]);
			minRange = Integer.parseInt(args[1]);
			maxRange = Integer.parseInt(args[2]);
			if (args.length == 4) {
				numThreads = Integer.parseInt(args[3]);
			}

		} else {
			logger.log(Level.INFO, "Parameters are - codeDepth minRange maxRange [numThreads] - using defaults.");
		}

		logger.log(Level.INFO, "numThreads: {0} - codeDepth: {1} - minRange: {2} - maxRange: {3}", new Object[] {
				numThreads, codeDepth, minRange, maxRange });

		logger.log(Level.INFO, "Starting Grid Example");
		ActorPool pool = ActorPool.getInstance(numThreads);
		UUID quizMaster = pool.spawnActor(com.akyumen.actor.example.grid.QuizMaster.class, UUID.randomUUID());
		// MUST startMessageServer ONLY after at least one actor exists
		pool.startMessageServer();
		pool.sendMessage(new QuizMasterInitMsg(quizMaster, quizMaster, codeDepth, minRange, maxRange));
		pool.sendMessage(new QuizMasterGuessMsg(quizMaster, quizMaster));
		pool.blockUntilFinished();
		logger.log(Level.INFO, "Finished Grid Example");

		// MUST restart pool - as terminates when there are no actors left
		if (pool.restart()) {
			logger.log(Level.INFO, "Starting Tree Example");
			quizMaster = pool.spawnActor(com.akyumen.actor.example.tree.QuizMaster.class, UUID.randomUUID());
			// MUST startMessageServer ONLY after at least one actor exists
			pool.startMessageServer();
			pool.sendMessage(new QuizMasterInitMsg(quizMaster, quizMaster, codeDepth, minRange, maxRange));
			pool.sendMessage(new QuizMasterGuessMsg(quizMaster, quizMaster));
			pool.blockUntilFinished();
			logger.log(Level.INFO, "Finished Tree Example");
		}

		// MUST restart pool - as terminates when there are no actors left
		if (pool.restart()) {
			logger.log(Level.INFO, "Starting Recursive Example");
			quizMaster = pool.spawnActor(com.akyumen.actor.example.recursive.QuizMaster.class, UUID.randomUUID());
			// MUST startMessageServer ONLY after at least one actor exists
			pool.startMessageServer();
			pool.sendMessage(new QuizMasterInitMsg(quizMaster, quizMaster, codeDepth, minRange, maxRange));
			pool.sendMessage(new QuizMasterGuessMsg(quizMaster, quizMaster));
			pool.blockUntilFinished();
		}
		logger.log(Level.INFO, "Finished Recursive Example");

		logger.log(Level.INFO, "Finished Demo");

	}

}
