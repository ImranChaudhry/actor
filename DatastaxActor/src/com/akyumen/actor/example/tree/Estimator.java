package com.akyumen.actor.example.tree;

import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.akyumen.actor.example.grid.message.EstimatorInitMsg;
import com.akyumen.actor.example.tree.Message.EstimateRequestMsg;
import com.akyumen.actor.example.tree.Message.EstimateResponseMsg;
import com.akyumen.actor.impl.definition.iActorPool;
import com.akyumen.actor.impl.messages.Message;
import com.akyumen.actor.impl.messages.SelfDestructMsg;

public class Estimator extends com.akyumen.actor.example.grid.Estimator {
	static {
		logger = Logger.getLogger("com.akyumen.actor.example2");
	}

	public Estimator(UUID uuid, UUID parent, iActorPool pool) {
		super(uuid, parent, pool);
	}

	@Override
	public boolean processRequest(Message msg) throws Throwable {
		if (msg instanceof EstimateRequestMsg) {
			logger.log(Level.FINE, "EstimateRequestMsg Received " + msg);
			EstimateRequestMsg erQ = (EstimateRequestMsg) msg;
			int depth = erQ.getDepth();
			if (guess != null) { // initial step - ignore when no guess as I am
									// a consolidator and not an estimator
				depth--;
			}
			if (depth == 0) {

				EstimateResponseMsg outMsg = new EstimateResponseMsg(getUuid(), msg.getSource());
				outMsg.updateGuess(erQ.getEstimate(), guess);
				logger.log(Level.FINER, getUuid()+" - Depth is 0 sending EstimateResponseMsg to QuizMaster " + msg.getSource()
						+ " with guess of " + outMsg.getEstimateString());
				sendMessage(outMsg);

			} else {
				spawnChildren();

				int range = codeMax - codeMin + 1;
				for (int i = 0; i < range; i++) {
					logger.log(Level.FINER, "Depth is " + depth + " sending EstimateRequestMsg to "
							+ childEstimators[i]);
					// lie and say the source is the quizmaster so he gets the
					// response direct
					EstimateRequestMsg outMsg = new EstimateRequestMsg(msg.getSource(), childEstimators[i], depth);
					outMsg.updateGuess(erQ.getEstimate(), guess);
					sendMessage(outMsg);
				}
			}
			return true;
		}
		return false;

	}
	
	protected void spawnChildren() {
		if (childEstimators == null) {
			synchronized (this) {
				if (childEstimators == null) {
					int range = codeMax - codeMin + 1;
					childEstimators = new UUID[range];
					for (int i = 0; i < range; i++) {
						childEstimators[i] = spawnActor(Estimator.class);
						logger.log(Level.FINER, "Spawning Estimator actor as a child : " + childEstimators[i]);
						sendMessage(new EstimatorInitMsg(getUuid(), childEstimators[i], codeMin, codeMax, codeMin + i));
					}
				}
			}
		}
	}

	@Override
	public boolean processResponse(Message msg) throws Throwable {
		// I should never get a response directly
		return false;
	}

	@Override
	public void notifyChildrenModification(UUID deadChild) {
		// Do Nothing I don't care if children die
		if (children.size() == 0) { // if no children - kill myself
			sendMessage(new SelfDestructMsg(getUuid(), getUuid()));
		}
	}

}
