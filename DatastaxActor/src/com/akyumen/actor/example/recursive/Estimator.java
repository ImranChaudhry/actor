package com.akyumen.actor.example.recursive;

import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.akyumen.actor.example.grid.message.EstimatorInitMsg;
import com.akyumen.actor.example.tree.Message.EstimateRequestMsg;
import com.akyumen.actor.example.tree.Message.EstimateResponseMsg;
import com.akyumen.actor.impl.definition.iActorPool;
import com.akyumen.actor.impl.messages.Message;

public class Estimator extends com.akyumen.actor.example.grid.Estimator {
	static {
		logger = Logger.getLogger("com.akyumen.actor.example2");
	}

	public Estimator(UUID uuid, UUID parent, iActorPool pool) {
		super(uuid, parent, pool);
	}

	@Override
	public boolean processRequest(Message msg) throws Throwable {
		if (msg instanceof EstimateRequestMsg) {
			logger.log(Level.FINE, "EstimateRequestMsg Received " + msg);
			EstimateRequestMsg erQ = (EstimateRequestMsg) msg;
			int depth = erQ.getDepth();
			if (guess == null) {
				// we are the estimator multiplexer - multiplex!
				int range = codeMax - codeMin + 1;
				for (int i = 0; i < range; i++) {
					logger.log(Level.FINER, "Estimator Multiplexer - Depth is " + depth
							+ " sending EstimateRequestMsg to " + childEstimators[i]);
					// lie and say the source is the quizmaster so he gets the
					// response direct
					EstimateRequestMsg outMsg = new EstimateRequestMsg(msg.getSource(), childEstimators[i], depth);
					outMsg.updateGuess(erQ.getEstimate(), guess);
					sendMessage(outMsg);
				}
			} else {
				depth--;
				if (depth == 0) { // we are the final layer of estimation -
									// return guess to quizmaster
					EstimateResponseMsg outMsg = new EstimateResponseMsg(getUuid(), msg.getSource());
					outMsg.updateGuess(erQ.getEstimate(), guess);
					logger.log(Level.FINER, "Depth is 0 sending EstimateResponseMsg to QuizMaster " + msg.getSource()
							+ "with guess of " + outMsg.getEstimateString());
					sendMessage(outMsg);
				} else { // Add my guess to the estimate and return to the
							// multiplexer to recurse
					EstimateRequestMsg outMsg = new EstimateRequestMsg(msg.getSource(), parent, depth);
					outMsg.updateGuess(erQ.getEstimate(), guess);
					sendMessage(outMsg);
				}

			}
			return true;
		}
		return false;

	}

	@Override
	public boolean initialise(Message msg) throws Throwable {
		if (msg instanceof EstimatorInitMsg) {
			EstimatorInitMsg qm = (EstimatorInitMsg) msg;
			codeMax = qm.getCodeMax();
			codeMin = qm.getCodeMin();
			guess = qm.getCodeGuess();
			if (guess == null) {// we are the root estimator - who manages the
								// other estimators
				spawnChildren();
			}
			logger.log(Level.FINER, "Initialising : " + msg);
			return true;
		} else {
			return false;
		}
	}

	protected void spawnChildren() {
		if (childEstimators == null) {
			synchronized (this) {
				if (childEstimators == null) {
					int range = codeMax - codeMin + 1;
					childEstimators = new UUID[range];
					for (int i = 0; i < range; i++) {
						childEstimators[i] = spawnActor(Estimator.class);
						logger.log(Level.FINER, "Spawning Estimator actor as a child : " + childEstimators[i]);
						sendMessage(new EstimatorInitMsg(getUuid(), childEstimators[i], codeMin, codeMax, codeMin + i));
					}
				}
			}
		}
	}

	@Override
	public boolean processResponse(Message msg) throws Throwable {
		// I should never get a response directly
		return false;
	}

	@Override
	public void notifyChildrenModification(UUID deadChild) {
		// Do Nothing I don't care if children die
	}

}
