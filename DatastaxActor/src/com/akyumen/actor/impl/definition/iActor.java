package com.akyumen.actor.impl.definition;

import com.akyumen.actor.impl.messages.Message;

public interface iActor {
	
	public boolean processRequest(Message msg) throws Throwable;
	public boolean processResponse(Message msg) throws Throwable;
	public boolean initialise(Message msg) throws Throwable;

}
