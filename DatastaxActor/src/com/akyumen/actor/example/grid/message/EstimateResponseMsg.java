package com.akyumen.actor.example.grid.message;

import java.util.LinkedList;
import java.util.UUID;

import com.akyumen.actor.impl.messages.Message;

public class EstimateResponseMsg extends Message {
	protected LinkedList<Integer> estimate;

	public LinkedList<Integer> getEstimate() {
		return estimate;
	}

	public EstimateResponseMsg(UUID source, UUID destination) {
		super(source, destination, MessageType.Response);
	}

	public void updateGuess(Integer guess) {
		estimate = new LinkedList<Integer>();
		if (guess != null) {
			estimate.addFirst(guess);
		}
	}

	
	public void updateGuess(LinkedList<Integer> childEstimate, Integer guess) {
		estimate = childEstimate;
		if (guess != null) {
			estimate.addFirst(guess);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("EstimateResponseMsg -" + super.toString() + " estimate:");
		if (estimate == null) {
			sb.append("null");
		} else {
			for (Integer est : estimate) {
				sb.append("[" + est + "]");
			}
		}
		return sb.toString();
	}
}
