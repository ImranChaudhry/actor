package com.akyumen.actor.example.grid.message;

import java.util.UUID;

import com.akyumen.actor.impl.messages.Message;

public class EstimateRequestMsg extends Message {
	protected int depth;

	public int getDepth() {
		return depth;
	}

	public EstimateRequestMsg(UUID source, UUID destination, int depth) {
		super(source, destination, MessageType.Request);
		this.depth=depth;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "EstimateRequestMsg - "+super.toString()+"  Depth:"+depth;
	}

	public MessageType getGuess() {
		// TODO Auto-generated method stub
		return null;
	}
}
