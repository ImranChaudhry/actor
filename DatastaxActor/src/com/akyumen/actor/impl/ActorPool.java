package com.akyumen.actor.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.akyumen.actor.impl.definition.iActorPool;
import com.akyumen.actor.impl.messages.ActorFailureMsg;
import com.akyumen.actor.impl.messages.Message;
import com.akyumen.actor.impl.messages.Message.AdvisoryNotification;

/**
 * Class which implements the ActorPool - runs the message delivery service as a
 * thread, spawns actors and adds them to its ActorPool - manages delivery of
 * messages and records when actors terminate and removes them from internal
 * directory.
 * 
 * @author imran
 *
 */

public class ActorPool implements iActorPool, Runnable {
	int numberThreads = 16;
	private static final Logger logger = Logger.getLogger("com.akyumen.actor.impl");

	// Fine for actors - as runs in green threads and will hyperthread between
	// the actors
	protected ActorExecutor actorExecutor;
	// Concurrent because any actor can post a message (simultaneously)
	protected ConcurrentLinkedQueue<Message> messages = null;
	// Concurrent because any actor can spawn an actor (simultaneously)
	protected ConcurrentHashMap<UUID, Actor> actors = null;
	protected Set<UUID> deadPool = null; // synchronised - see constructor

	protected volatile boolean messageServerActive = false;
	protected static volatile ActorPool instance = null;

	/*
	 * Singleton access for ActorPool Takes parameter but only used on when
	 * Instance is not yet constructed otherwise will ignore the parameter
	 */
	public static ActorPool getInstance(int numberThreads) {
		if (instance == null) {
			synchronized (ActorPool.class) {
				if (instance == null) {
					logger.log(Level.FINEST, "Constructing");
					instance = new ActorPool(numberThreads);
				}
			}
		}
		return instance;
	}

	public boolean restart() {
		logger.log(Level.FINEST, "Restart called on ActorPool");
		if (!messageServerActive) {
			actorExecutor = new ActorExecutor(numberThreads - 1);
			messages.clear();
			deadPool.clear();
			logger.log(Level.FINEST, "Restart suceeded on ActorPool");
			return true;
		}
		logger.log(Level.FINEST, "Restart failed on ActorPool");
		return false;
	}

	protected ActorPool(int numThreads) {
		this.numberThreads = numThreads;
		// -1 as we need a thread for the mailman
		actorExecutor = new ActorExecutor(numberThreads - 1);

		messages = new ConcurrentLinkedQueue<Message>();
		// NUMBER_THREADS - reasonable guess for number actors initially
		actors = new ConcurrentHashMap<UUID, Actor>(numberThreads);
		deadPool = Collections.synchronizedSet(new HashSet<UUID>(numberThreads));

	}

	@Override
	/**
	 * Sticks messages being sent on the processing queue
	 */
	public void sendMessage(Message msg) {
		logger.log(Level.FINEST, "Received Message: " + msg);
		messages.add(msg);
	}

	public void startMessageServer() {
		if (!messageServerActive) {
			synchronized (ActorPool.class) {
				if (!messageServerActive) {
					logger.log(Level.FINEST, "Starting Message Server");
					// one time thread which runs continually till there are no
					// actors and exits no need to keep a handle on it - NOT in
					// the executor pool as this needs to run continually and
					// not be preempted randomly by actors.
					Thread t = new Thread(this);
					t.setName("Mail Daemon");
					t.start();
					messageServerActive = true;
				}
			}
		}
	}

	@Override
	/**
	 * Obituary mechanism for Actor so it can be removed from the children
	 */
	public boolean notifyActorDeath(UUID uuid) {
		// only self can initiate removal and messages can only be sent from
		// parent
		// or self to demand termination - so no need to notify.
		logger.log(Level.FINEST, "Obituary recived for " + uuid + " removed from cache");
		synchronized (deadPool) { // maintain a set of deadActors
			deadPool.add(uuid);
		}
		return actors.remove(uuid) != null;
	}

	@Override
	/** Spawns Actors for the specified type and returns their UUID
	 */
	public UUID spawnActor(Class<?> actor, UUID parentUuid) {
		try {
			// get the ONLY constructor we accept for an actor
			// all other initialisation needs to happen be sending an initialise
			// msg to the spawned actor
			Constructor<?> ctr;
			ctr = actor.getConstructor(new Class[] { UUID.class, UUID.class, iActorPool.class });
			UUID childUuid = UUID.randomUUID();
			Actor child = (Actor) ctr.newInstance(new Object[] { childUuid, parentUuid, this });
			// created actor so then add it to executor service and actor
			// directory and return
			actors.put(childUuid, child);
			actorExecutor.schedule(child);
			logger.log(Level.FINER, "Spawned actor of type " + child.getClass().getName() + " with UUID " + childUuid);
			return childUuid;

		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			// we failed - should never happen so does not need to be
			// communicated via a message
			// but we should print the stacktrace.
			logger.log(Level.SEVERE, "Unable to Spawned actor " + e.getMessage());
			return null;
		}

	}

	@Override
	/**
	 * Does nothing but pools message queue to deliver all the messages and
	 * exit once all the actors are dead
	 */
	public void run() {

		// while we have some actors (i.e. system is present)
		// => presupposes we spawn the initial actor BEFORE we start the pool
		while (!actors.isEmpty()) {
			Message msg;
			while ((msg = messages.poll()) != null) {
				// get the target actor
				UUID dest = msg.getDestination();
				Actor actor = actors.get(dest);
				if (actor == null) { // unknown actor
					String unknownActorStatus = "unknown";
					if (deadPool.contains(dest)) {
						unknownActorStatus = "terminated";
					}
					// actor doesn't exist - tell the sender
					if (msg instanceof AdvisoryNotification) {
						// prevents infinite loops - you've terminated the
						// parent and child so the child tells the parent (which
						// no longer exists) and that caused an error msg
						// which gets sent to the child (which no longer exists)
						// ad infinitum
						logger.log(Level.FINEST, "AdvisoryNotification received for " + unknownActorStatus + " actor -"
								+ msg);
					} else {
						String msgContents = "Message received for " + unknownActorStatus + " actor -" + msg;
						logger.log(Level.WARNING, msgContents);
						sendMessage(new ActorFailureMsg(msg.getDestination(), msg.getSource(), msgContents));
					}
				} else {
					// deliver message to actor's queue
					logger.log(Level.FINEST, "Message routed to Actor: " + actor + " Message :" + msg);
					actor.receiveMessage(msg);
				}
			}
		}
		// finish execution as all actors are dead
		actorExecutor.shutdown();
		messageServerActive = false;
		return;
	}

	public void blockUntilFinished() {
		//last thing we do before we exit
		while (messageServerActive) {
		}
	}
}
