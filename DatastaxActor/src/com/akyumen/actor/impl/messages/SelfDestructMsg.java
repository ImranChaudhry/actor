package com.akyumen.actor.impl.messages;

import java.util.UUID;

public class SelfDestructMsg extends Message {

	public SelfDestructMsg(UUID source, UUID destination) {
		super(source, destination, Message.MessageType.Infrastructure);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "SelfDestructMsg "+super.toString();
	}

}
