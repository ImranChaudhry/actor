#ActorDemo for DataStax.#

## Infrastructure design ##

###*com.akyumen.actor.impl*###

###Actor###

This is the abstract actor implementation which all actors must extend.
The methods the actor must implement are in the iActor interface - essentially processingRequestMessages, processingResponseMessages  and processingInitMessages.

All Actors are spawned and managed by the ActorPool which uses a predefined constructor which
allows not passing of parameters to a spawned actor.
Spawned actors must be sent an InitMessage to set any parameters they require.

Implementation of Actor's can not terminate themselves directly but must send themselves
SelfDestruct messages which are handled by the abstract actor implementation.

The abstractActorImplementation has a method (executeMessages) which when called checks if there are any messages waiting for the actor - if there are it processes them (all or a maximum number of
messages per iteration.)
If for any reason (SelfDestruct / Exceptions) the Actor needs to terminate it returns false which means
do not schedule me again for execution, true means reschedule me.

There is a PriorityBlockingQueue to handle incoming messages - which are ordered according to
their Message type INFRASTRUCTURE> INIT>RESPONSE>REQUEST so that higher priority 
messages are processed first, and as per the Actor definition the Actor message processing is not
FIFO.

###ActorExecutor###
This is the threadpool manager for the Actors - it wraps a FixedThreadPool.

Because the numbers of actors can be very large - 1000s, having them run in their own threads
is not practical nor scalable, and thread starvation issues occur.

Instead extending the paradigm that an Actor can process a message then be paused, the
ActorExecutor initiates upto the specified number of ActorExecuting threads (typically 16) and takes
and Actor from the ConcurrentQueue (FIFO for actors) and executes it.

Each Actor on completion either asks to be rescheduled (returns true) or indicates it has finished (false) and is removed from the ActorQueue.

Some heurestic attempt at controlling the number of ActorExecutor threads and number of pending Actors occurs.


###ActorPool###

This manages the ActorExecutor and controls spawning of actors  and message delivery.

####Spawning of Actors####
This registers the Actor's address (a UUID which also acts as a capability as per the Actor definition),
with the mail service so messages can be received and schedules the Actor with the ActorExecutor for processing.
Spawning a child returns the child's UUID and not a handle to the Actor directly.

####Message Delivery####
Actor's can not communicate directly to one another and all communication is via Messages.
Messages are sent with a source (which in actuality can be spoofed and is in reality the UUID of the Actor who should receive the Response to a Request) and a destination UUID.

The messages are pushed into a ConcurrentLinkedQueue (FIFO)  and a mail thread takes each message finds the destination Actor and delivers the message. If the Actor no longer exists (has self destructed, etc) it sends an advisory back to the sender of the message that the message could not be delivered.

The reason that there is a large unprocessed queue of messages in the ActorPool and a smaller pending queue in the Actor is:
1. There is priority ordering of messages in the Actor.
2. There is overhead savings in having the Actor have the ability to process through all of its pending
messages in one execution opposed to be able to only execute one message if it had no incoming message queue.

###Messages###
###*com.akyumen.actor.impl.messages*###

####Message####
This is the superclass for messages and has basic information such as message type ( INFRASTRUCTURE INIT RESPONSE REQUEST), source and destination UUIDs and a comparator for ordering

####UnknownRequestMsg####
If an Actor receives a message it does not know how to handle - it returns false in its processResponse/Request method - in turn the abstract Actor superclass sends an UnknownRequest message to the misbehaving requesting Actor - which in turn terminates that actor.

####SelfDestructMsg####
If an Actor needs to be terminated a SelfDestructMsg is sent to it  - either by itself or from anybody who knows its UUID. It terminates and notifies its parents and destroy requester (with an ActorFailureMsg) and in turn tells any of its children to SelfDestruct also - and removes itself from the mail directory.

####ActorFailureMsg####
This message indicates an Actor has failed/self destructed - its main purpose is for a child to inform its parent actor that it has died - so the parent can spawn a replacement if required or take any appropriate steps.

##Example##

*Executable class is com.akyumen.actor.ActorDemo*

It demonstrates the Actor Infrastructure with three example - each solving the same problem.

You guess a n digit code - say 5789 where the number of digits is 4 and the alphabet for each
digit ranges from 4 to 9 in this case.

You then generate actors to estimate a single digit and collectively guess the code.

###Tree example###

This example has the QuizMaster actor spawn a root Estimator actor.

And EstimatorRequest message is sent to the root Estimator.

This root estimator recursively spawns 6 Child Estimator actors (4-9 is 6 characters in our alphabet.)
Each Estimator is allocated a character which it will guess (EstimatorInitMessage) and an EstimatorResquest message is sent.

Each child estimator then recursively spawn 6 Child Estimators - until we reach the required depth (4 levels under the root Estimator.)

Finally the leaf Estimator guesses its character and sends a message that to its parent Estimator (EstimateResponse message)

The parent Estimator appends its character to the guess from the child Estimator and sends a message with it to its parent. (EstimateResponse message)

This occurs recursively until it reaches the QuizMaster - who checks the code - if it is correct it tells the root Estimator to self destruct and terminates itself.

The root Estimator self destructs and tells its children to do so, which recursively terminates the tree.

###Grid example###

This is similar to the Tree example but with two important differences.

When each parent Estimator spawns a child Estimator - it adds its guess into the EstimatorRequest message - so that the child Estimator knows the guess of the levels above it.

This is different to the Tree example where the guess was built up in the EstimatorResponse messages initiated by the leaf Estimators, in this case it is passed down in the EstimatorRequest messages.

The leaf Estimators then send the completed guess directly to the QuizMaster in an EstimatoResponse message.

The QuizMaster then checks the guess - and sends a SelfDestruct message to the leaf Estimator who sent the estimate.

Once a parent Estimator's children (the leaf Estimators) are all dead - it also self-destructs, this recurses up and cleans up the tree.

If the guess is accurate - the QuizMaster sends a self destruct to the root Estimator and destroys the
grid from above.

###Recursive Example.###

This is a modification of the Grid Example

Opposed to spawning repeated layers of Estimators - the root Estimator spawns one layer of children Estimators.

Each child Estimator (as per the Grid Example) inserts its guess into the EstimateRequest message and sends that message to its PARENT the Root Estimator.

The Root Estimator recursively passes all the EstimateRequest messages to its children for the required number of depths - wherein the guess is sent to the QuizMaster

The QuizMaster checks the guesses, and on a correct guess sends a self-destroy to the Root Estimator.