package com.akyumen.actor.impl;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.akyumen.actor.impl.definition.iActor;
import com.akyumen.actor.impl.definition.iActorExecutable;
import com.akyumen.actor.impl.definition.iActorPool;
import com.akyumen.actor.impl.messages.ActorFailureMsg;
import com.akyumen.actor.impl.messages.Message;
import com.akyumen.actor.impl.messages.SelfDestructMsg;
import com.akyumen.actor.impl.messages.UnknownRequestMsg;
import com.akyumen.actor.impl.messages.Message.MessageType;

/**
 * Implements the Actor Pattern Can only be communicated by Messages Can only
 * send messages to other Actors it is told about (and its parent) Can be
 * terminated by sending a SelfDestruct message
 * 
 * No need to synchronisation or thread safety as each instance is only run
 * serially.
 * 
 * @author imran
 *
 */
public abstract class Actor implements iActor, iActorExecutable {
	private static final Logger logger = Logger.getLogger("com.akyumen.actor.impl");

	protected UUID uuid;
	protected UUID parent;

	// The only reason that we have this boolean is that an inherited class can
	// overwrite it to true if it doesn't require any initialisation messages to
	// be sent.
	protected volatile boolean initialised = false;
	protected iActorPool pool;
	protected Set<UUID> children = null;

	protected PriorityBlockingQueue<Message> messages = null;
	// multiple mailmen can deliver (theoretically) though I only have one
	// mailman runnning.

	// allows us to yield execution after each X messages if the processing is
	// expensive
	protected int maxMessagesPerIteration;

	// requests/responses on the msg queuewhen we
	// know we are going to selfdestruct to await
	// that msg

	public Actor(UUID uuid, UUID parent, iActorPool pool) {
		this(uuid, parent, pool, Integer.MAX_VALUE);
	}

	public Actor(UUID uuid, UUID parent, iActorPool pool, int maxMessagesPerIteration) {
		logger.log(Level.FINEST, "Constructor called for : {0} Parent {1}", new Object[] { uuid, parent });
		this.maxMessagesPerIteration = maxMessagesPerIteration;
		this.uuid = uuid;
		this.parent = parent;
		this.pool = pool;
		// only I can write to my children in a single thread
		// => not synchronised
		children = new HashSet<UUID>();

		// Priority Blocking Queue so we can order the msgs by priority and
		// to handle that we can be reading a msg whilst the mailman is
		// delivering a msg => syncronization
		messages = new PriorityBlockingQueue<Message>();
	}

	//	package as should be called from ActorPool
	void receiveMessage(Message msg) {
		messages.put(msg);

	}
	
	//protected as can be called internally in subclasses (visible)
	protected UUID getUuid() {
		return uuid;
	}
	
	//protected as can be called internally in subclasses (visible)
	protected UUID getParent() {
		return parent;
	}
	//protected as can be called internally in subclasses (visible)
	protected void sendMessage(Message msg) {
		pool.sendMessage(msg);
	}

	/*
	 * Spawn a child actor associated with me
	 * 	protected as can be called internally in subclasses (visible)
	 */
	protected UUID spawnActor(Class<?> actor) {
		UUID child = pool.spawnActor(actor, uuid);
		children.add(child);
		logger.log(Level.FINEST, "Spawn Actor now {0} children", children.size());
		return child;
	}

	/*
	 * Logic which processes messages and calls the appropriate process
	 * functions and handles any exceptions and failures - nicely. Part of Actor
	 * pattern is handling failure cleanly. ONLY every run serially - not in
	 * parallel
	 * 
	 * @see com.akyumen.actor.impl.definition.iActorExecutable#executeMessages()
	 */
	public boolean executeMessages() {
		Message msg = null;
		for (int i = 0; i < maxMessagesPerIteration; i++) {
			try {
				msg = messages.poll();
				if (msg == null) { // we haven't got anything left to do - just
									// return so we can be rescheduled
					return true;
				}
				// msg is guaranteed not null herein forwards
				if (msg instanceof SelfDestructMsg) {
					return selfDestruct(msg.getSource(), "SelfDestructMsg rcvd : " + msg.toString());
				}

				if (!initialised) {
					// Actor requires initialisation with initialise message
					// first
					if (msg.getMsgType() == MessageType.Initialise) {
						initialised = initialise(msg); // Exceptions
														// throwable
						logger.log(Level.FINER, "Actor {0} initialised by {1} ", new Object[] {
								(initialised ? "" : "NOT"), msg });
					} else {
						// put the message back on the queue - and wait for
						// initialise
						receiveMessage(msg);

						return true; // release the thread and ask to be
										// rescheduled
					}
				} else { // initialised
					switch (msg.getMsgType()) {
					case Request:
						if (!processRequest(msg)) // Exceptions throwable
						{
							sendUnknownRequestMsg(msg);
						}
						break;
					case Response:
						if (!processResponse(msg)) // Exceptions throwable
						{
							sendUnknownRequestMsg(msg);
						}
						break;
					case Infrastructure:
						if (msg instanceof ActorFailureMsg) {
							// only infrastructure method which will not
							// cause a self destruct
							if (children.remove(msg.getSource())) {
								// only notify if we actually remove a
								// child
								notifyChildrenModification(msg.getSource());
							}
							break;
						} else if (msg instanceof UnknownRequestMsg) {
							// misbehaving - selfDestruct
							return selfDestruct(msg.getSource(), "Sent Unknown Request message :" + msg.toString());
						} else {// unknown msg - tell sender
							sendUnknownRequestMsg(msg);
							break;
						}
					case Initialise:
						return selfDestruct(msg.getSource(),
								"Already initialised, can not be reinitialised : " + msg.toString());
					}
				}
			} catch (Throwable t) {
				return selfDestruct(msg.getSource(), t.toString());
			}
		}
		// reschedule me
		return true;
	}

	public abstract void notifyChildrenModification(UUID deadChild);

	//private as can only be called internally not in subclasses (hidden)
	private void sendUnknownRequestMsg(Message sourceMsg) {
		sendMessage(new UnknownRequestMsg(sourceMsg.getDestination(), sourceMsg.getSource(), sourceMsg.toString()));
	}

	/*
	 * Implements self-termination private as can only be called internally -
	 * anything which extends Actor should send itself a SelfDestruct message
	 */

	private boolean selfDestruct(UUID failSource, String failMsg) {

		String reason = "Self Destruct initiated : " + failMsg;
		logger.log(Level.FINER, reason);
		// tell my parent that I'm self destructing
		sendMessage(new ActorFailureMsg(uuid, parent, reason));
		// tell my children to self destruct
		for (UUID child : children) { // send time to die to
										// children
			sendMessage(new SelfDestructMsg(uuid, child));
		}
		// tell the pool to remove me from cache
		pool.notifyActorDeath(uuid);
		Message queuedMsg;
		while ((queuedMsg = messages.poll()) != null) {
			// send failure message for all pending messages -
			// reason why I disabled myself
			// so as to prevent trying to process any other
			// message whilst awaiting a self-destruct
			sendMessage(new ActorFailureMsg(uuid, queuedMsg.getSource(), reason));
		}

		if (failSource != null) {
			// explained we threw an exception to caller
			sendMessage(new ActorFailureMsg(uuid, failSource, failMsg));
		}
		return false;
	}
}
