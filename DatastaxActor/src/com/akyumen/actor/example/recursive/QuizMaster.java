package com.akyumen.actor.example.recursive;

import java.util.LinkedList;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.akyumen.actor.example.grid.message.EstimatorInitMsg;
import com.akyumen.actor.example.grid.message.QuizMasterGuessMsg;
import com.akyumen.actor.example.tree.Message.EstimateRequestMsg;
import com.akyumen.actor.example.tree.Message.EstimateResponseMsg;
import com.akyumen.actor.impl.definition.iActorPool;
import com.akyumen.actor.impl.messages.Message;
import com.akyumen.actor.impl.messages.SelfDestructMsg;

/**
 * Recursive  example - makes a estimate multiplexer with a group of estimators which guess a level
 * at a time, each Guess Requests recurses back to the estimate multiplexer so the next level can
 * be guessed, each time appending the guess to guessRequest and at the last second of the guess 
 * is returned directly to the QuizMaster. 
 * Once the code is correctly guessed - it terminates the multiplexer estimater which then causes
 * itself and the group of estimators to be terminated.
 */

public class QuizMaster extends com.akyumen.actor.example.grid.QuizMaster {
	static {
		logger = Logger.getLogger("com.akyumen.actor.example3");
	}

	public QuizMaster(UUID uuid, UUID parent, iActorPool pool) {
		super(uuid, parent, pool);
	}
	@Override
	public boolean processRequest(Message msg) throws Throwable {
		if (msg instanceof QuizMasterGuessMsg) {
			logger.log(Level.FINER, "QuizMasterGuessMsg received" + msg);
			if (child == null) { // initialise on first guess only
				child = spawnActor(Estimator.class);
				sendMessage(new EstimatorInitMsg(getUuid(), child, codeMin, codeMax, null));
			}
			sendMessage(new EstimateRequestMsg(getUuid(), child, codeLength));
			return true;
		}
		return false;
	}

	@Override
	public boolean processResponse(Message msg) throws Throwable {
		if (msg instanceof EstimateResponseMsg) {
			logger.log(Level.FINER, "EstimateResponseMsg received " + msg);
			EstimateResponseMsg erMsg = (EstimateResponseMsg) msg;
			boolean found = true;
			LinkedList<Integer> guess = erMsg.getEstimate();
			for (int i = 0; i < codeLength; i++) {
				if (guess.get(i) != code[i]) {
					found = false;
					break;
				}
			}
			if (found) {
				StringBuilder sb = new StringBuilder();
				sb.append("Code Match : ");
				for (int num : code) {
					sb.append("[" + num + "]");
				}
				logger.log(Level.INFO, sb.toString());
				// kill the estimator
				sendMessage(new SelfDestructMsg(getUuid(), child));
			}
			return true;
		}
		return false;

	}
	
	@Override
	public void notifyChildrenModification(UUID deadChild) {
		if (children.size() == 0) { // if no children - kill myself
			sendMessage(new SelfDestructMsg(getUuid(), getUuid()));
		}
	}

}
