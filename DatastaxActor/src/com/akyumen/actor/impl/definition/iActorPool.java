package com.akyumen.actor.impl.definition;

import java.util.UUID;

import com.akyumen.actor.impl.messages.Message;

public interface iActorPool {
	void sendMessage(Message msg);
	UUID spawnActor(Class<?> actor, UUID parent);
	boolean notifyActorDeath(UUID uuid);

}
